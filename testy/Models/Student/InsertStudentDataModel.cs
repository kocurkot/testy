﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace testy.Models.Student
{
    public class InsertStudentDataModel
    {
        [Required]
        [Display(Name = "Imie")]
        public string ImieStudent { get; set; }
        [Required]
        [Display(Name = "Nazwisko")]
        public string NazwiskoStudent { get; set; }
    }
}