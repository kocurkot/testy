﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace testy.Models.Student
{
    public class DataToGenerateQuestion : DataToGenerateTest
    {
        public string QuestionContent { get; set; }
        public List<String> Odpowiedzi { get; set; }
        public int IndexOfQuestion { get; set; }
        public string QuestionType { get; set; }
        
        
    }
}