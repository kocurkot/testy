﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace testy.Models.Student
{
    public class AnswerData
    {
        public Dictionary<int,String> Answers { get; set; }
    }
}