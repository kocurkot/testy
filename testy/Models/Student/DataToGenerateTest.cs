﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace testy.Models.Student
{
    public class DataToGenerateTest
    {
        public decimal WynikiId { get; set; }
        public decimal TestId { get; set; }
        public long CountOfQuestionsToGenerate { get; set; }
    }
}