﻿using System.ComponentModel.DataAnnotations;

namespace testy.Models.Student
{
    public class PassStudentToTestModel
    {
        [Required]
        [Display(Name = "Nazwa testu")]
        public string NazwaTestu { get; set; }
        [Required]
        [Display(Name = "Grupa")]
        public string Grupa { get; set; }
        [Required]
        [Display(Name = "Haslo")]
        public string Haslo { get; set; }
    }
}