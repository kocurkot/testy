﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace testy.Models
{
    public class PokazWynikiModel
    {
        [Required(ErrorMessage = "Musisz podać wartość")]
        [StringLength(20, ErrorMessage = "Zbyt długa nazwa")]
        public string NazwaGrupy { get; set; }
    }
}