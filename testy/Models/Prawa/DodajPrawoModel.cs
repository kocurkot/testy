﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Net.Mime;
using System.Web;
using System.Web.Mvc;

namespace testy.Models
{
    public class DodajPrawoModel
    {

        public int PDT_WybranyTest { get; set; }
        public int ileWypelniono { get; set; }
        public int idPrawa { get; set; }
        public List<SelectListItem> PDT_Testy { get; set; }

        [Required(ErrorMessage = "Musisz podać wartość")]
        [StringLength(10, ErrorMessage = "Zbyt długa nazwa")]
        public string PDT_nazwaGrupy { get; set; }

        [Required(ErrorMessage = "Błędna wartość")]
        public int PDT_limitWypelnien { get; set; }

        [Required(ErrorMessage = "Błędna wartość")]
        public int PDT_iloscPytan { get; set; }

        [Required(ErrorMessage = "Błędna wartość")]
        public DateTime PDT_czasRozpoczecia { get; set; }

        [Required(ErrorMessage = "Błędna wartość")]
        public DateTime PDT_czasZakonczenia { get; set; }

        [Required(ErrorMessage = "Musisz podać wartość")]
        [StringLength(10, ErrorMessage = "Zbyt długie hasło")]
        public string PDT_haslo { get; set; }

        [Required(ErrorMessage = "Błędna wartość")]
        public int PDT_czasNaWykonanieWMinutach { get; set; }

        public int PDT_test_ID_FK { get; set; } 
    }
}