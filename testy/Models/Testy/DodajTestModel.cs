﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace testy.Models
{
    public class DodajTestModel
    {
        public string TES_haslo { get; set; }
        public DateTime TES_dataUtworzenia { get; set; }
        public bool TES_czyAktywny { get; set; }
        public int TES_idWlasciciela { get; set; }
        public int TES_czasTrwaniaWMin { get; set; }
        public int TES_iloscPytan { get; set; }
        [Required(ErrorMessage = "Musisz podać nazwę")]
        [StringLength(50, ErrorMessage = "Zbyt długa nazwa")]
        public string TES_nazwa { get; set; }
        public int TES_idTest { get; set; }
    }
}