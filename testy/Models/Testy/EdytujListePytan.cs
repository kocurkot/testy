﻿ using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace testy.Models.Testy
{
    public class EdytujListePytan
    {
        public int idTest { get; set; }
        public string pytaniaZaznaczoneNowe { get; set; }
        public List<Pytania> wszystkiePytania { get; set; }
        public List<decimal> pytaniaZaznaczone { get; set; } 
    }
}