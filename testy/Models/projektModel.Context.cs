﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace testy.Models
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Infrastructure;
    
    public partial class projektEntities : DbContext
    {
        public projektEntities()
            : base("name=projektEntities")
        {
        }
    
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            throw new UnintentionalCodeFirstException();
        }
    
        public virtual DbSet<Odpowiedzi> Odpowiedzi { get; set; }
        public virtual DbSet<PrawaDoTestu> PrawaDoTestu { get; set; }
        public virtual DbSet<Pytania> Pytania { get; set; }
        public virtual DbSet<test> test { get; set; }
        public virtual DbSet<UdzieloneOdpowiedzi> UdzieloneOdpowiedzi { get; set; }
        public virtual DbSet<wlascicielTestu> wlascicielTestu { get; set; }
        public virtual DbSet<Wyniki> Wyniki { get; set; }
        public virtual DbSet<XXX_test_pytania> XXX_test_pytania { get; set; }
    }
}
