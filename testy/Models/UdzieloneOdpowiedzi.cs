//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace testy.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class UdzieloneOdpowiedzi
    {
        public decimal UDZ_ID { get; set; }
        public decimal UDZ_Wyniki_FK { get; set; }
        public decimal UDZ_Pytania_FK { get; set; }
        public string Odpowiedz { get; set; }
    
        public virtual Pytania Pytania { get; set; }
        public virtual Wyniki Wyniki { get; set; }
    }
}
