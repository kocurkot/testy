﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace testy.Models
{
    public class DodajPytanieModel
    {
        public int PYT_IdPytania { get; set; }
        public int IdTestu { get; set; }
        public string submit { get; set; }
        public bool PYT_czyAktywne { get; set; }

        [Required(ErrorMessage = "Musisz podać wartość")]
        public string PYT_tresc { get; set; }
        public string PYT_typ { get; set; }

        [Required(ErrorMessage = "Musisz podać wartość")]
        public int PYT_iloscPunktow { get; set; }

        public List<string> odpowiedz { get; set; }
        public string odpowiedzOtwarta { get; set; }
        public List<string> zaznaczone { get; set; }
        public List<int> odpowiedzId { get; set; }
    }
}