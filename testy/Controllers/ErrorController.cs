﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using testy.Models;

namespace testy.Controllers
{
    public class ErrorController : Controller
    {
        [HttpGet]
        public ActionResult Index()
        {
            ViewBag.errorMessage = Session["errorMessage"] == null ? "coś poszło nie tak... spróbuj jeszcze raz :)" : Session["errorMessage"].ToString();
            Session["errorMessage"] = null;
            return View();
        }
    }
}