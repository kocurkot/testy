﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using testy.Helpers;
using testy.Models;

namespace testy.Controllers.Student
{
    public class EndOfTestController : Controller
    {
        // GET: EndOfTest
        public ActionResult Index()
        {
            
            decimal wynId = 0;
            if(Session["WYN_ID"] != null)
                wynId = decimal.Parse(Session["WYN_ID"].ToString());
            Session.Abandon();
            Session["endOfTest"] = true;
            var points = ResultsHelper.GetPointsResults(wynId);
            ViewBag.points = points;

            using (var dbc = new projektEntities())
            {
                var wynikRow = (from wyn in dbc.Wyniki
                    where wyn.WYN_ID.Equals(wynId)
                    select wyn).FirstOrDefault();
                if (wynikRow != null)
                {
                    //update database
                    wynikRow.WYN_punkty = points;
                    dbc.Wyniki.Attach(wynikRow);
                    dbc.Entry(wynikRow).State = EntityState.Modified;
                    dbc.SaveChanges();
                }
            }


            return View();
        }

    }
}