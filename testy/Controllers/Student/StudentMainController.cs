﻿using System;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using System.Web.UI;
using Microsoft.Ajax.Utilities;
using testy.Helpers;
using testy.Models;
using testy.Models.Student;

namespace testy.Controllers.Student
{
    public class StudentMainController : Controller  
    {
        // GET: /StudentMain/

        private RedirectToRouteResult ReturnToEndOfTest()
        {
                return RedirectToAction("Index", "EndOfTest");
        }

        public ActionResult Index()
        {
            
            
            if (Session["PassIntoStartTest"] != null && Session["endOfTest"] == null)
            {
                
                var dataToGenerateTest = new DataToGenerateTest
                {
                     WynikiId = decimal.Parse(Session["WYN_ID"].ToString()), 
                     TestId = decimal.Parse(Session["testId"].ToString()), 
                     CountOfQuestionsToGenerate = TestHelper.GetAmountOfQuestionsToGenerate(decimal.Parse(Session["testId"].ToString()), Session["groupName"].ToString())
                };
                TestHelper.GetAllActiveQuestionsFromTest(dataToGenerateTest.TestId);
                TestHelper.SetEndTime(); //temp time
                
                if (IsTimeOut()) return ReturnToEndOfTest();
                return View(dataToGenerateTest);
            }
            return RedirectToAction("Logowanie", "StudentVeryficate");
        }

      
        
        public ActionResult GenerateNextQuestion(DataToGenerateQuestion dataToGenerateQuestion)
        {
            if (IsTimeOut()) return ReturnToEndOfTest();
            TestHelper.GenerateQuestionData(dataToGenerateQuestion, false);
            return PartialView("GenerateQuestion", dataToGenerateQuestion);
        }

        public ActionResult GeneratePreviousQuestion(DataToGenerateQuestion dataToGenerateQuestion)
        {
            if (IsTimeOut()) return ReturnToEndOfTest();
            TestHelper.GenerateQuestionData(dataToGenerateQuestion, true);
            return PartialView("GenerateQuestion", dataToGenerateQuestion);
        }

        [HttpPost]
        public void SaveAnswers(string[] answers, decimal wynikId, decimal pytanieId)
        {
            if (!CheckThatAnswerIsInDb(wynikId, pytanieId) && !IsTimeOut() && answers[0] != "error")
            {
                string result = "";
                foreach (string answer in answers)
                    result = result + answer.ToString();

                var udzieloneOdpowiedzi = new UdzieloneOdpowiedzi
                {
                    UDZ_Wyniki_FK = wynikId,
                    UDZ_Pytania_FK = pytanieId,
                    Odpowiedz = result
                };
                using (var dbc = new projektEntities())
                {
                    dbc.UdzieloneOdpowiedzi.Add(udzieloneOdpowiedzi);
                    dbc.SaveChanges();
                }
            }
        }

        [HttpPost]
        public bool CheckThatAnswerIsInDb(decimal wynikId, decimal pytanieId)
        {
            using (var dbc = new projektEntities())
            {
                var result = (from c in dbc.UdzieloneOdpowiedzi
                    where c.UDZ_Pytania_FK.Equals(pytanieId)
                          && c.UDZ_Wyniki_FK.Equals(wynikId)
                    select c).FirstOrDefault();

                if (result != null) 
                    return true;
                else 
                    return false;
            }
        }

        

        [HttpPost]
        public int CheckRemainingTime()
        {
            if (Session["end"] == null) return 0;
            var checkRemainingTime = (int) ((DateTime) Session["end"] - DateTime.Now).TotalSeconds;
            return checkRemainingTime;
        }

        private bool IsTimeOut()
        {
            if (Session["end"] == null) return true;
            if ((DateTime) Session["end"] < DateTime.Now)
                return true;
            return false;
        }

        
        
	}




}