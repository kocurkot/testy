﻿using System;
using System.Data.Entity;
using System.Data.Entity.Core;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using testy.Helpers;
using testy.Models;
using testy.Models.Student;

namespace testy.Controllers.Student
{
    public class StudentVeryficateController : Controller
    {
        // GET: Student
        [HttpGet]
        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public ActionResult Logowanie()
        {
            if (Session["WYN_ID"] != null) return RedirectToAction("Index", "StudentMain"); //jesli test już trwa
            return View();
        }

        [HttpPost]
        public ActionResult Logowanie(PassStudentToTestModel test)
        {
            
            if (ModelState.IsValid)
            {
                try
                {
                    using (var dbc = new projektEntities())
                    {
                        var testData = (from tes in dbc.test
                                          join pdt in dbc.PrawaDoTestu on tes.TES_nrTestu equals pdt.PDT_test_ID_FK
                                         where tes.TES_nazwa.Equals(test.NazwaTestu) &&
                                               tes.TES_aktywny &&
                                               pdt.PDT_nazwaGrupy.Equals(test.Grupa) &&
                                               pdt.PDT_haslo.Equals(test.Haslo)
                                         select new
                                         {
                                             pdt.PDT_id,
                                             pdt.PDT_nazwaGrupy,
                                             tes.TES_nrTestu,
                                             tes.TES_nazwa
                                         }).FirstOrDefault();

                        if (testData == null) {Session.Abandon(); return View(test); }

                        SessionStudentHelper.SetActualPdtRowId(testData.PDT_id);
                        var permission =
                            new PerrmisionToSolveTestValidator(
                                decimal.Parse(SessionStudentHelper.GetActualPdtRowId().ToString(CultureInfo.InvariantCulture)));

                        if (!permission.CheckPermissions()) { Session.Abandon(); return View(test); }

                        Session["groupName"] = testData.PDT_nazwaGrupy;
                        Session["testId"] = testData.TES_nrTestu;
                        Session["testName"] = testData.TES_nazwa;
                        Session["PassIntoInsertStudentData"] = "true";
                        

                        return RedirectToAction("InsertStudentData");
                    }
                }
                catch (EntityException ex)
                {
                    Session["errorMessage"] = ex.Message;
                    return RedirectToAction("Index", "Error");
                }
                catch (Exception ex)
                {
                    Session["errorMessage"] = ex.Message;
                    return RedirectToAction("Index", "Error");
                }

                
            }
            return View();
        }

        private void IncrementAmountOfSolvedTests()
        {
            using (var dbc = new projektEntities())
            {
                var actualRowId = SessionStudentHelper.GetActualPdtRowId();
                var pdtRow = (from pdt in dbc.PrawaDoTestu
                              where pdt.PDT_id == actualRowId
                              select pdt).FirstOrDefault();
                if (pdtRow != null)
                {
                    decimal amountTemp = Decimal.Parse(pdtRow.PDT_ileWypelniono.ToString());
                    pdtRow.PDT_ileWypelniono = amountTemp + 1;
                    dbc.PrawaDoTestu.Attach(pdtRow);
                    dbc.Entry(pdtRow).State = EntityState.Modified;
                    dbc.SaveChanges();
                }
                


            }
        }

        [HttpGet]
        public ActionResult InsertStudentData()
        {
            if (Session["PassIntoInsertStudentData"] != null)
            {
                Session["PassIntoInsertStudentData"] = null;
                return View();
            }
            return RedirectToAction("Logowanie");
        }

        [HttpPost]
        public ActionResult InsertStudentData(InsertStudentDataModel studentData)
        {
            if (ModelState.IsValid)
            {
                using (var dbc = new projektEntities())
                {
                    Session["PassIntoStartTest"] = "true";
                    
                    var wyniki = new Wyniki
                    {
                        WYN_imieStudenta = studentData.ImieStudent,
                        WYN_nazwiskoStudenta = studentData.NazwiskoStudent,
                        WYN_grupaStudenta = Session["groupName"].ToString(),
                        WYN_test_ID_FK = decimal.Parse(Session["testId"].ToString()),
                        WYN_ip = IpAddressHelper.GetIpAddress()
                    };

                    try
                    {
                        dbc.Wyniki.Add(wyniki);
                        dbc.SaveChanges();
                        Session["WYN_ID"] = wyniki.WYN_ID;
                    }
                    catch (EntityException ex)
                    {
                        Session["errorMessage"] = ex.Message;
                        return RedirectToAction("Index", "Error");
                    }
                    catch (Exception ex)
                    {
                        Session["errorMessage"] = ex.Message;
                        return RedirectToAction("Index", "Error");
                    }
                    IncrementAmountOfSolvedTests();
                    return RedirectToAction("StartTest");
                }
            }
            return View();
        }


        public ActionResult StartTest()
        {
            if (Session["PassIntoStartTest"] != null)
            {
                return View();
            }
            return RedirectToAction("InsertStudentData");
        }

        
        
    }
}