﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.UI;
using testy.Models;
using testy.Helpers.Admin;

namespace testy.Controllers.Admin
{
    public class PrawaController : Controller
    {
        [HttpGet]
        public ActionResult DodajNowe()
        {
            if (Session["Id"] != null)
            {
                DodajPrawoModel dane = new DodajPrawoModel();
                using (var db = new projektEntities())
                {
                    PrawaConHelper.PobierzTestyZbazy(db, dane, int.Parse(Session["Id"].ToString()));
                }
                return View(dane);
            }
            return RedirectToAction("Logowanie", "Admin");
        }


        [HttpPost]
        public ActionResult DodajNowe(DodajPrawoModel daneZFormularza)
        {
            if (Session["Id"] != null)
            {
                if (ModelState.IsValid)
                {
                    using (var db = new projektEntities())
                    {
                        PrawaDoTestu doZapisu = new PrawaDoTestu();
                        PrawaConHelper.ZapiszPrawo(daneZFormularza, doZapisu, db);
                        return RedirectToAction("Zarzadzaj");
                    }
                }
                using (var db = new projektEntities())
                {
                    PrawaConHelper.PobierzTestyZbazy(db, daneZFormularza, int.Parse(Session["Id"].ToString()));
                }
                return View(daneZFormularza);
            }
            return RedirectToAction("Logowanie", "Admin");
        }

        [HttpGet]
        public ActionResult Edytuj(int id)
        {
            if (Session["Id"] == null)
                return RedirectToAction("Logowanie", "Admin");

            int idWlasciciela = int.Parse(Session["Id"].ToString());
            DodajPrawoModel prawoDlaFormularza = new DodajPrawoModel();

            using (var db = new projektEntities())
            {
               var znalezionePrawoDoTestu = (from i in db.PrawaDoTestu
                    join e in db.test on i.PDT_test_ID_FK equals e.TES_nrTestu
                    where i.PDT_id == id && e.TES_wlasciciel_ID_FK == idWlasciciela
                    select i).FirstOrDefault();
                if (znalezionePrawoDoTestu == null)
                    return RedirectToAction("Zarzadzaj");
                PrawaConHelper.WypelnijModelPrawaDoEdycji(db, prawoDlaFormularza, idWlasciciela, znalezionePrawoDoTestu);
            }
            return View(prawoDlaFormularza);
        }

        [HttpPost]
        public ActionResult Edytuj(DodajPrawoModel daneZFormularza)
        {
            if (Session["Id"] == null)
                return RedirectToAction("Logowanie", "Admin");
            PrawaDoTestu daneDoUpdate = new PrawaDoTestu();
            if (ModelState.IsValid)
            {
                PrawaConHelper.UpdatePrawaPrzepiszDaneZForm(daneZFormularza, daneDoUpdate);
                using (var db = new projektEntities())
                {
                    db.PrawaDoTestu.AddOrUpdate(daneDoUpdate);
                    db.SaveChanges();
                    return RedirectToAction("Zarzadzaj");
                }
            }
            using (var db = new projektEntities())
            {
                PrawaConHelper.PobierzTestyZbazy(db, daneZFormularza, int.Parse(Session["Id"].ToString()));
            }
            return View(daneZFormularza);
        }

        [HttpGet]
        public ActionResult Usun(int id)
        {
            if (Session["Id"] == null)
                return RedirectToAction("Logowanie", "Admin");

            using (var db = new projektEntities())
            {
                int idWlasciciela = int.Parse(Session["Id"].ToString());
                var czyIstniejePrawo = (from i in db.PrawaDoTestu
                    join e in db.test on i.PDT_test_ID_FK equals e.TES_nrTestu
                    where i.PDT_id == id && e.TES_wlasciciel_ID_FK == idWlasciciela
                    select i).FirstOrDefault();
                if (czyIstniejePrawo == null)
                    return RedirectToAction("Zarzadzaj");
                db.PrawaDoTestu.Remove(czyIstniejePrawo);
                db.SaveChanges();
            }
            return RedirectToAction("Zarzadzaj");
        }

        [HttpGet]
        public ActionResult Zarzadzaj()
        {
            if (Session["Id"] == null)
                return RedirectToAction("Logowanie", "Admin");

            List<PrawaDoTestu> prawaZTabeli = new List<PrawaDoTestu>();
            using (var db = new projektEntities())
            {
                int idWlasciciela = int.Parse(Session["Id"].ToString());
                PrawaConHelper.PrawaNalezaceDoUzytkownika(db, idWlasciciela, prawaZTabeli);
            }
            return View(prawaZTabeli);
        }
        
        [HttpGet]
        public ActionResult ZarzadzajPrawami()
        {
            if (Session["Id"] == null)
                return RedirectToAction("Logowanie", "Admin");
            return View();
        }
	}
}