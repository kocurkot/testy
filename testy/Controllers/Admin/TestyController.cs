﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Web;
using System.Web.Mvc;
using testy.Helpers.Admin;
using testy.Models;
using testy.Models.Testy;

namespace testy.Controllers.Admin
{
    public class TestyController : Controller
    {
        [HttpGet]
        public ActionResult DodajNowy()
        {
            if (CzyMaPrawaAdmina(Role.Admin))
                return RedirectToAction("Logowanie", "Admin");
            return View();
        }

        [HttpPost]
        public ActionResult DodajNowy(DodajTestModel daneZFormularza)
        {
            if (CzyMaPrawaAdmina(Role.Admin))
                return RedirectToAction("Logowanie", "Admin");
            if (ModelState.IsValid)
            {
                int idWlasciciela = int.Parse(Session["Id"].ToString());
                TestyConHelper.ZapisanieTestuWBazie(daneZFormularza, idWlasciciela);
                return RedirectToAction("Zarzadzaj");
            }
            return View(daneZFormularza);
        }

        [HttpGet]
        public ActionResult EdytujListePytan(int id)
        {
            if (CzyMaPrawaAdmina(Role.Admin))
                return RedirectToAction("Logowanie", "Admin");
            int idWlasciciela = int.Parse(Session["Id"].ToString());
            EdytujListePytan listaPytanDlaWidoku = new EdytujListePytan();
            using (var db = new projektEntities())
            {
                var czyDobreId = (from i in db.test
                    where i.TES_wlasciciel_ID_FK == idWlasciciela
                          && i.TES_aktywny == true
                          && i.TES_nrTestu == id
                    select i).FirstOrDefault();

                if (czyDobreId == null)
                    return RedirectToAction("Zarzadzaj");

                listaPytanDlaWidoku.idTest = id;
                listaPytanDlaWidoku.wszystkiePytania = (from c in db.Pytania
                    where c.PYT_wlascicielTestu_ID_FK == idWlasciciela
                          && c.PYT_czyAktywne == true
                    select c).ToList();

                listaPytanDlaWidoku.pytaniaZaznaczone = (from d in db.XXX_test_pytania
                    where d.XTP_test_ID == id
                    select d.XTP_pytania_ID).ToList();
            }
            if (listaPytanDlaWidoku.pytaniaZaznaczone == null)
                listaPytanDlaWidoku.pytaniaZaznaczone = new List<decimal>();
            if (listaPytanDlaWidoku.wszystkiePytania == null)
                listaPytanDlaWidoku.wszystkiePytania = new List<Pytania>();
            return View(listaPytanDlaWidoku);
        }

        [HttpPost]
        public ActionResult EdytujListePytan(EdytujListePytan daneZFormularza)
        {
            if (CzyMaPrawaAdmina(Role.Admin))
                return RedirectToAction("Logowanie", "Admin");
            int idWlasciciela = int.Parse(Session["Id"].ToString());
            using (var db = new projektEntities())
            {
                var czyIstnieje = (from i in db.test
                    where i.TES_wlasciciel_ID_FK == idWlasciciela
                          && i.TES_nrTestu == daneZFormularza.idTest
                          && i.TES_aktywny == true
                    select i).FirstOrDefault();
                if (czyIstnieje == null)
                    return RedirectToAction("Zarzadzaj");
                var stare = (from c in db.XXX_test_pytania
                            where c.XTP_test_ID == daneZFormularza.idTest
                            select c.XTP_pytania_ID).ToList();
                var dlugoscStringuZIdPytan = daneZFormularza.pytaniaZaznaczoneNowe.Length;
                string[] idPytanZFormularza = daneZFormularza.pytaniaZaznaczoneNowe.Substring(0, dlugoscStringuZIdPytan - 1).Split(',');
                foreach (var item in stare)
                {
                    if (TestyConHelper.SprawdzCzyUsunac(idPytanZFormularza, item.ToString()))
                    {
                        var doUsuniecia = (from d in db.XXX_test_pytania
                            where d.XTP_pytania_ID == item
                                  && d.XTP_test_ID == daneZFormularza.idTest
                            select d).FirstOrDefault();
                        db.XXX_test_pytania.Remove(doUsuniecia);
                    }
                }
                db.SaveChanges();
                foreach (var item in idPytanZFormularza)
                {
                    if (TestyConHelper.CzyDodacNowe(stare, item))
                    {
                        var doDodania = new XXX_test_pytania();
                        doDodania.XTP_pytania_ID = int.Parse(item);
                        doDodania.XTP_test_ID = daneZFormularza.idTest;
                        db.XXX_test_pytania.Add(doDodania);
                    }
                }
                db.SaveChanges();
            }
            return RedirectToAction("Zarzadzaj");
        }


        [HttpGet]
        public ActionResult EdytujTest(int id)
        {
            if (CzyMaPrawaAdmina(Role.Admin))
                return RedirectToAction("Logowanie", "Admin");

            int idWlasciciela = int.Parse(Session["Id"].ToString());
            DodajTestModel testDoEdycji;
            testDoEdycji = TestyConHelper.ZwrocDaneTestuZBazy(id, idWlasciciela);
            if (testDoEdycji == null)
                return RedirectToAction("Zarzadzaj");
            return View(testDoEdycji);
        }

        [HttpPost]
        public ActionResult EdytujTest(DodajTestModel daneZFormularza)
        {
            if (CzyMaPrawaAdmina(Role.Admin))
                return RedirectToAction("Logowanie", "Admin");

            if (ModelState.IsValid)
            {
                using (var db = new projektEntities())
                {
                    int idWlasciciela = int.Parse(Session["Id"].ToString());
                    test daneDoUpdatu = (from i in db.test
                        where i.TES_nrTestu == daneZFormularza.TES_idTest
                              && i.TES_wlasciciel_ID_FK == idWlasciciela
                        select i).FirstOrDefault();

                    if(daneDoUpdatu == null)
                        return RedirectToAction("Zarzadzaj");

                    daneDoUpdatu.TES_nazwa = daneZFormularza.TES_nazwa;

                    db.test.AddOrUpdate(daneDoUpdatu);
                    db.SaveChanges();
                }
                return RedirectToAction("Zarzadzaj");
            }
            return View(daneZFormularza);
        }

        [HttpGet]
        public ActionResult Usun(int id)
        {
            if (CzyMaPrawaAdmina(Role.Admin))
                return RedirectToAction("Logowanie", "Admin");
            int idWlasciciela = int.Parse(Session["Id"].ToString());
            TestyConHelper.UnieaktywnijWybranyTest(id, idWlasciciela);
            return RedirectToAction("Zarzadzaj");
        }

        [HttpGet]
        public ActionResult Zarzadzaj()
        {
            if (CzyMaPrawaAdmina(Role.Admin))
                return RedirectToAction("Logowanie", "Admin");

            List<DodajTestModel> testyZBazy = new List<DodajTestModel>();
            int idWlasciciela = int.Parse(Session["Id"].ToString());
            TestyConHelper.ZwrocTestyZbazy(idWlasciciela, testyZBazy);

            return View(testyZBazy);
        }

        [HttpGet]
        public ActionResult ZarzadzajTestami()
        {
            if (CzyMaPrawaAdmina(Role.Admin))
                return RedirectToAction("Logowanie", "Admin");
            return View();
        }

        private bool CzyMaPrawaAdmina(Role ob)
        {
            return Session["Rola"] != null && !Session["Rola"].ToString().Equals(ob.ToString());
        }
	}
}