﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Microsoft.Ajax.Utilities;
using testy.Helpers;
using testy.Helpers.Admin;
using testy.Models;

namespace testy.Controllers.Admin
{
    public class PytaniaController : Controller
    {
        [HttpGet]
        public ActionResult DodajNowe()
        {
            if (Session["Id"] == null)
                return RedirectToAction("Logowanie", "Admin");
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult DodajNowe(DodajPytanieModel daneZFormularza)
        {
            if (Session["Id"] != null)
            {
                if (ModelState.IsValid)
                {
                    using (var db = new projektEntities())
                    {
                        int idWlasciciela = int.Parse(Session["Id"].ToString());
                        PytaniaConHelper.zapiszPytanieWBazie(db, daneZFormularza, idWlasciciela);
                        if(daneZFormularza.submit == "Dodaj pytanie")
                            return RedirectToAction("DodajNowe");
                        else
                            return RedirectToAction("ZarzadzajPytaniami");
                    }
                }
                return View(daneZFormularza);
            }
            return RedirectToAction("Logowanie");
        }

        [HttpGet]
        public ActionResult Edytuj(int id)
        {
            if (Session["Id"] == null)
                return RedirectToAction("Logowanie", "Admin");
            DodajPytanieModel pytanieDlaWidoku = new DodajPytanieModel();
            int idWlasciciela = int.Parse(Session["Id"].ToString());
            PytaniaConHelper.ZwrocPytaniaDoEdycji(id, idWlasciciela, pytanieDlaWidoku);
            return View(pytanieDlaWidoku);
        }

        [HttpPost]
        public ActionResult Edytuj(DodajPytanieModel daneZFormularza)
        {
            if (Session["Id"] == null)
                return RedirectToAction("Logowanie", "Admin");
            if (ModelState.IsValid)
            {
                int idWlasciciela = int.Parse(Session["Id"].ToString());
                PytaniaConHelper.ZapiszEdytowanePytanieWBazie(daneZFormularza, idWlasciciela);
                return RedirectToAction("Zarzadzaj");
            }
            return View(daneZFormularza);
        }


        [HttpGet]
        public ActionResult Usun(int id)
        {
            if (Session["Id"] == null)
                return RedirectToAction("Logowanie", "Admin");
            int idWlasciciela = int.Parse(Session["Id"].ToString());
            using (var db = new projektEntities())
            {
                var czyIstniejePytanieDoUsuniecia = (from i in db.Pytania
                    where i.PYT_wlascicielTestu_ID_FK == idWlasciciela
                          && i.PYT_ID == id
                    select i).FirstOrDefault();
                if (czyIstniejePytanieDoUsuniecia == null)
                    return RedirectToAction("Zarzadzaj");
                czyIstniejePytanieDoUsuniecia.PYT_czyAktywne = false;
                db.Pytania.AddOrUpdate(czyIstniejePytanieDoUsuniecia);
                db.SaveChanges();
            }
            return RedirectToAction("Zarzadzaj");
        }
        
        [HttpGet]
        public ActionResult Zarzadzaj()
        {
            if (Session["Id"] == null)
                return RedirectToAction("Logowanie", "Admin");
            List<DodajPytanieModel> pytaniaDlaWidoku = new List<DodajPytanieModel>();
            int idWlasciciela = int.Parse(Session["Id"].ToString());
            PytaniaConHelper.PobierzPytaniaZBazy(idWlasciciela, pytaniaDlaWidoku);
            return View(pytaniaDlaWidoku);

        }


        [HttpGet]
        public ActionResult ZarzadzajPytaniami()
        {
            if (Session["Id"] == null)
                return RedirectToAction("Logowanie", "Admin");

            return View();
        }
	}
}