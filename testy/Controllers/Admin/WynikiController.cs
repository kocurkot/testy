﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using testy.Helpers.Admin;
using testy.Models;

namespace testy.Controllers.Admin
{
    public class WynikiController : Controller
    {
        [HttpGet]
        public ActionResult ZarzadzajWynikami()
        {
            if (CzyMaPrawaAdmina(Role.Admin))
                return RedirectToAction("Logowanie", "Admin");
            return View();
        }

        [HttpGet]
        public ActionResult WedlugGrupy()
        {
            if (CzyMaPrawaAdmina(Role.Admin))
                return RedirectToAction("Logowanie", "Admin");
            return View();
        }

        [HttpPost]
        public ActionResult WedlugGrupy(PokazWynikiModel daneZFormularza)
        {
            if (CzyMaPrawaAdmina(Role.Admin))
                return RedirectToAction("Logowanie", "Admin");
            return RedirectToAction("Wyniki/" + daneZFormularza.NazwaGrupy);
        }


        [HttpGet]
        public ActionResult Wyniki(string id = null)
        {
            if (CzyMaPrawaAdmina(Role.Admin))
                return RedirectToAction("Logowanie", "Admin");
            int idWlasciciela = int.Parse(Session["Id"].ToString());

            List<Wyniki> wszystkieWyniki;
            using (var db = new projektEntities())
            {
                if (id == null)
                {
                    wszystkieWyniki = (from i in db.Wyniki
                                       join d in db.test on i.WYN_test_ID_FK equals d.TES_nrTestu
                                       where d.TES_wlasciciel_ID_FK == idWlasciciela
                                       orderby i.WYN_ID descending
                                       select i).ToList();
                }else
                {
                    wszystkieWyniki = (from i in db.Wyniki
                                       join d in db.test on i.WYN_test_ID_FK equals d.TES_nrTestu
                                       where d.TES_wlasciciel_ID_FK == idWlasciciela
                                       && i.WYN_grupaStudenta == id
                                       orderby i.WYN_ID descending
                                       select i).ToList();
                }
            }
            if (wszystkieWyniki == null)
                wszystkieWyniki = new List<Wyniki>();
            return View(wszystkieWyniki);
        }

        private bool CzyMaPrawaAdmina(Role ob)
        {
            return Session["Rola"] != null && !Session["Rola"].ToString().Equals(ob.ToString());
        }
    }
}