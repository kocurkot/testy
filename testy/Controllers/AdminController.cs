﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Web.Mvc;
using testy.Helpers;
using testy.Helpers.Admin;
using testy.Models;

namespace testy.Controllers
{
    public class AdminController : Controller
    {
        [HttpGet]
        public ActionResult Index()
        {
            if (Session["Rola"].ToString().Equals(Role.Admin.ToString()))
            {
                return View();
            }
            return RedirectToAction("Logowanie");
        }

        [HttpGet]
        public ActionResult Logowanie()
        {
            if (Session["Rola"] != null)
            {
                return RedirectToAction("Index");
            }
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Logowanie(wlascicielTestu wlasciciel)
        {
            if (ModelState.IsValid)
            {
                using (var db = new projektEntities())
                {
                    var user = (from c in db.wlascicielTestu
                                where c.WLA_login.Equals(wlasciciel.WLA_login)
                                && c.WLA_haslo.Equals(wlasciciel.WLA_haslo)
                                && c.WLA_czyAktywny == true
                                select c).FirstOrDefault();

                    if (user == null) return View(wlasciciel);

                    Session["Id"] = user.WLA_ID.ToString();
                    Session["Login"] = user.WLA_login.ToString();
                    Session["Rola"] = Role.Admin.ToString();
                    return RedirectToAction("Index");
                }
            }
            return View(wlasciciel);
        }

        [HttpGet]
        public ActionResult Wylogowanie()
        {
            Session["Id"] = null;
            Session["Login"] = null;
            Session["Rola"] = null;
            return RedirectToAction("Index", "Home");
        }


        private bool sprawdzCzyJestWlascicielemTestu(projektEntities db, int IdTestu)
        {
            int idZalogowanego = int.Parse(Session["Id"].ToString());
            var czyJestWlascicielem = (from i in db.test
                                      where i.TES_nrTestu == IdTestu
                                      select i.TES_wlasciciel_ID_FK == idZalogowanego).FirstOrDefault();
            return czyJestWlascicielem;
        }

        private void zapiszPytanieWBazie(projektEntities db, DodajPytanieModel pytanie)
        {
            var pytanieDoZapisu = new Pytania();
            int idWlasciciela = int.Parse(Session["Id"].ToString());
            pytanieDoZapisu.PYT_iloscPunktow = pytanie.PYT_iloscPunktow;
            pytanieDoZapisu.PYT_tresc = pytanie.PYT_tresc;
            pytanieDoZapisu.PYT_typ = pytanie.PYT_typ;
            pytanieDoZapisu.PYT_wlascicielTestu_ID_FK = idWlasciciela;
            db.Pytania.Add(pytanieDoZapisu);
            db.SaveChanges();

            var idOstatniegoPytania = (from i in db.Pytania
                                       where i.PYT_wlascicielTestu_ID_FK == idWlasciciela
                                       orderby i.PYT_ID descending 
                                       select i.PYT_ID).FirstOrDefault();

            if (pytanie.PYT_typ == "Otwarte")
            {
                var odpDoZapisu = new Odpowiedzi();
                odpDoZapisu.ODP_pyt_ID_FK = idOstatniegoPytania;
                odpDoZapisu.ODP_tresc = pytanie.PYT_tresc;
                odpDoZapisu.ODP_czyPoprawne = true;
                db.Odpowiedzi.Add(odpDoZapisu);
            }
            else
            {
                int ktora = 0;
                foreach (var item in pytanie.odpowiedz)
                {
                    ktora++;
                    if (!String.IsNullOrEmpty(item))
                    {
                        var odpDoZapisu = new Odpowiedzi();
                        odpDoZapisu.ODP_pyt_ID_FK = idOstatniegoPytania;
                        odpDoZapisu.ODP_tresc = item;
                        if (pytanie.zaznaczone.Contains(ktora.ToString()))
                        {
                            odpDoZapisu.ODP_czyPoprawne = true;
                        }
                        else
                        {
                            odpDoZapisu.ODP_czyPoprawne = false;
                        }
                        db.Odpowiedzi.Add(odpDoZapisu);
                    }
                }
            }
            db.SaveChanges();
            var powiazanieTestuIPytania = new XXX_test_pytania();
            powiazanieTestuIPytania.XTP_pytania_ID = idOstatniegoPytania;
            powiazanieTestuIPytania.XTP_test_ID = pytanie.IdTestu;
            db.XXX_test_pytania.Add(powiazanieTestuIPytania);
            db.SaveChanges();
        }
    }
}