﻿function setDateTimePicker(buttonId, fieldId) {
    $(buttonId).click(
        function(e) {
            $(fieldId).AnyTime_noPicker().AnyTime_picker().focus();
            e.preventDefault();
        });
}