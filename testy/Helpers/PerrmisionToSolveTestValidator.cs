﻿using System;
using System.Linq;
using testy.Models;

namespace testy.Helpers
{
    public class PerrmisionToSolveTestValidator
    {
        private decimal _actualPdtId;
        private PrawaDoTestu  _pdt;
        public PerrmisionToSolveTestValidator(decimal actualPdtId)
        {
            this._actualPdtId = actualPdtId;
            GetDataFromDb();
        }

        public bool CheckPermissions()
        {
            if (ValidateFillLimit() && ValidateTimeInterval())
                return true;
            return false;
        }

        private void GetDataFromDb()
        {
            using (var dbc = new projektEntities())
            {
                _pdt = (from pdt in dbc.PrawaDoTestu
                    where pdt.PDT_id.Equals(_actualPdtId)
                    select pdt).FirstOrDefault();
            }
        }

        private bool ValidateFillLimit()
        {
            if (_pdt.PDT_ileWypelniono < _pdt.PDT_limitWypelnien)
            {
                return true;
            }
            return false;
        }

        private bool ValidateTimeInterval()
        {
            var actualTime = DateTime.Now;
            if (actualTime < _pdt.PDT_czasZakonczenia && actualTime > _pdt.PDT_czasRozpoczecia)
            {
                return true;
            }
            return false;
        }
    }
}