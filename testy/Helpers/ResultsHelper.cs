﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using Microsoft.Ajax.Utilities;
using testy.Models;

namespace testy.Helpers
{
    public static class ResultsHelper
    {
        private static bool ConvertChar01IntoBool(char c)
        {
            if (c == '1') return true;
            return false;
        }

        public static int GetPointsResults(decimal wynId)
        {
            int points = 0;
            using (var dbc = new projektEntities())
            {
                var studentResults = (from udz in dbc.UdzieloneOdpowiedzi
                    where udz.UDZ_Wyniki_FK.Equals(wynId) 
                    select udz).ToList();

                var answersFromBaseList = new List<List<Odpowiedzi>>();
                foreach (UdzieloneOdpowiedzi udz in studentResults)
                {
                     answersFromBaseList.Add((
                         from odp in dbc.Odpowiedzi
                         join pyt in dbc.Pytania on odp.ODP_pyt_ID_FK equals pyt.PYT_ID
                               where odp.ODP_pyt_ID_FK.Equals(udz.UDZ_Pytania_FK) 
                             select odp).ToList()
                                       ); 

                }
                bool goodAnswer = false;
                int answerIndexTemp = 0;
                foreach (UdzieloneOdpowiedzi udz in studentResults)
                {

                    for (int j = 0; j < answersFromBaseList[answerIndexTemp].Count; j++)
                    {
                        if (answersFromBaseList[answerIndexTemp][0].Pytania.PYT_typ == "Wielokrotnego wyboru" ||
                            answersFromBaseList[answerIndexTemp][0].Pytania.PYT_typ == "Jednokrotnego wyboru"){

                        for (int i = 0; i < udz.Odpowiedz.Length; i++)
                        {

                            if (ConvertChar01IntoBool(udz.Odpowiedz[i]) ==
                                answersFromBaseList[answerIndexTemp][i].ODP_czyPoprawne)
                            {
                                goodAnswer = true;
                            }
                            else
                            {
                                goodAnswer = false;
                                i = udz.Odpowiedz.Length - 1;
                            }

                        }
                    }
                        else if (answersFromBaseList[answerIndexTemp][0].Pytania.PYT_typ == "Otwarte")
                        {
                           string[] trueAnswers = answersFromBaseList[answerIndexTemp][0].ODP_tresc.Split(',');
                            foreach (string trueAnswer in trueAnswers)
                            {
                                int a = trueAnswer.Length;
                                string c = udz.Odpowiedz;
                                if (udz.Odpowiedz.Equals(trueAnswer) )
                                {
                                    goodAnswer = true;
                                }
                            }
                        }
                }

                    
                    if (goodAnswer)
                    {
                        points = points + Int32.Parse(answersFromBaseList[answerIndexTemp][0].Pytania.PYT_iloscPunktow.ToString());
                    }
                    answerIndexTemp++;
                    goodAnswer = false;
                }
               
            }
            return points;
        }

        public static int GetAmoutOfAllAvilablePoints()
        {
            using (var dbc = new projektEntities())
            {

                var questionsList = TestHelper.GetAllActiveQuestionsFromTest(SessionStudentHelper.GetActualTestId());
                var randomList = (List<int>)HttpContext.Current.Session["randomNumerQuestionsList"];
                decimal allPoints = 0;
                foreach (var questionNumber in randomList)
                {
                    allPoints += questionsList[questionNumber].PYT_iloscPunktow;
                }
                return Int32.Parse(allPoints.ToString());
            }
        }
    }

    
}