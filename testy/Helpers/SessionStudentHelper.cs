﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace testy.Helpers
{
    public static class SessionStudentHelper
    {
        public static void SetActualPdtRowId(decimal actualId)
        {
            HttpContext.Current.Session["PDT_id"] = actualId;
        }

        public static decimal GetActualPdtRowId()
        {
            if (HttpContext.Current.Session["PDT_id"] != null)
                return decimal.Parse(HttpContext.Current.Session["PDT_id"].ToString());
            return -1;
        }

        public static string GetActualGroupName()
        {
            if (HttpContext.Current.Session["groupName"] != null)
                return HttpContext.Current.Session["groupName"].ToString();
            return null;
        }

        public static decimal GetActualTestId()
        {

            if (HttpContext.Current.Session["testId"] != null)
                return decimal.Parse(HttpContext.Current.Session["testId"].ToString());
            return -1;
        }
    }
}