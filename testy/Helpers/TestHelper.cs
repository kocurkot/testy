﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Core.Common.CommandTrees;
using System.Linq;
using System.Web;
using System.Web.ModelBinding;
using System.Web.Mvc;
using System.Web.Routing;
using testy.Models;
using testy.Models.Student;
namespace testy.Helpers
{
    public static class TestHelper
    {
        public static List<Pytania> GetAllActiveQuestionsFromTest(decimal testId)
        {
            using (var dbc = new projektEntities())
            {
                return
                    (dbc.Pytania.SqlQuery("SELECT * " +
                                          "FROM Pytania t1 " +
                                          "INNER JOIN XXX_test_pytania t2 ON t1.PYT_ID = t2.XTP_pytania_ID AND t2.XTP_test_ID = "+testId+
                                          "WHERE t1.PYT_czyAktywne=1"))
                        .ToList();
            }
        }

        public static Pytania GetQuestionById(decimal testId, decimal questionId)
        {
            using (var dbc = new projektEntities())
            {
                var question = (from t in dbc.test
                                 from q in dbc.Pytania
                                 where t.TES_nrTestu == testId &&
                                 q.PYT_ID == questionId
                                 select q).FirstOrDefault();

                return question;
            }
        }

        public static long GetAmountOfQuestionsToGenerate(decimal testId, string nazwaGrupy)
        {
            //jesli bedziemy walidowac tak zeby pytan do losowania bylo <= niz dostepnych to sie poprawi to...
            //using (var dbc = new projektEntities())
            //{
            //    var test = (from t in dbc.PrawaDoTestu
            //                    where t.PDT_test_ID_FK == testId &&t.PDT_nazwaGrupy == nazwaGrupy 

            //                    select t).FirstOrDefault();

            //    return long.Parse(test.PDT_iloscPytan.ToString());
            //}
            return GetAllActiveQuestionsFromTest(testId).Count;
        }

        public static List<string> GetAnswersFromQuestionId(decimal questionId)
        {
            using (var dbc = new projektEntities())
            {
                var answers = (from a in dbc.Odpowiedzi
                    where a.ODP_pyt_ID_FK == questionId
                    select a.ODP_tresc).ToList();

                return answers;
            }
        }

        public static string GetQuestionType(decimal questionId)
        {
            using (var dbc = new projektEntities())
            {
                var type = (from q in dbc.Pytania
                    where q.PYT_ID.Equals(questionId)
                    select q.PYT_typ).FirstOrDefault();
                return type;
            }
            
        }

        public static void GenerateQuestionData(DataToGenerateQuestion dataToGenerateQuestion, bool previous)
        {
            dataToGenerateQuestion.TestId = decimal.Parse(HttpContext.Current.Session["testId"].ToString());

            var questionsList = GetAllActiveQuestionsFromTest(dataToGenerateQuestion.TestId);

            List<int> randomList;
            if (HttpContext.Current.Session["randomNumerQuestionsList"] == null)
            {
                
                var a = new Random(DateTime.Now.Ticks.GetHashCode());
                randomList = new List<int>();

                for (int i = 0; i < GetAmountOfQuestionsToGenerate(dataToGenerateQuestion.TestId,HttpContext.Current.Session["groupName"].ToString()); i++)
                {
                    int myNumber = a.Next(0, questionsList.Count);
                    if (!randomList.Contains(myNumber))
                        randomList.Add(myNumber);
                    else
                        i--;
                }

                HttpContext.Current.Session["randomNumerQuestionsList"] = randomList;
                HttpContext.Current.Session["timeIsSet"] = null;
                SetEndTime(); 
            }

            randomList = (List<int>)HttpContext.Current.Session["randomNumerQuestionsList"];
            int tempV = 0;
            if (HttpContext.Current.Session["currentIndexOfQuestion"] == null){
                HttpContext.Current.Session["currentIndexOfQuestion"] = 0;
            }
            else
            {
                tempV = int.Parse(HttpContext.Current.Session["currentIndexOfQuestion"].ToString());

                if (previous && tempV > 0)
                    tempV--;
                else if (!previous && tempV < randomList.Count - 1)
                    tempV++;


                HttpContext.Current.Session["currentIndexOfQuestion"] = tempV;
            }

            if (randomList != null)
            {
                var pytanie = questionsList[randomList[tempV]];
                dataToGenerateQuestion.QuestionContent = pytanie.PYT_tresc;
                HttpContext.Current.Session["PYT_ID"] = pytanie.PYT_ID;
                dataToGenerateQuestion.Odpowiedzi = GetAnswersFromQuestionId(pytanie.PYT_ID);
                dataToGenerateQuestion.QuestionType = GetQuestionType(pytanie.PYT_ID);
                if (tempV == randomList.Count)
                {
                    dataToGenerateQuestion.QuestionContent = "koniec testu!";
                }
            }
        }

        public static string GetStudentNameSurnameByWynikId(decimal wynId)
        {
            using (var dbc = new projektEntities())
            {
                var studentName = (from w in dbc.Wyniki
                    where w.WYN_ID.Equals(wynId)
                    select new
                    {
                        w.WYN_imieStudenta,
                        w.WYN_nazwiskoStudenta
                    }).FirstOrDefault();

                return studentName.WYN_imieStudenta + " " + studentName.WYN_nazwiskoStudenta;
            }
        }

        public static int GetRemainingTime(string nazwaGrupy)
        {
            using (var dbc = new projektEntities())
            {
                var rt = (from pdt in dbc.PrawaDoTestu
                    where pdt.PDT_nazwaGrupy.Equals(nazwaGrupy)
                    select pdt.PDT_czasNaRozwizanie).FirstOrDefault();

                if (rt != null) return (int) rt.Value;
                else throw new Exception("nie można pobrać czasu na wykonanie testu");
            }
        }

        public static int GetCurrentRemainingTime()
        {
            return (int)((DateTime) HttpContext.Current.Session["end"] - DateTime.Now).TotalSeconds;
        }

        public static void SetEndTime()
        {
            if (HttpContext.Current.Session["timeIsSet"] == null)
            {
                HttpContext.Current.Session["timeIsSet"] = true;
                var remainingTime = GetRemainingTime(HttpContext.Current.Session["groupName"].ToString());
                HttpContext.Current.Session["end"] = DateTime.Now.AddSeconds(remainingTime);
            }
        }

    }
}