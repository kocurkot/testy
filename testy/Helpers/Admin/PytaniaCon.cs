﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Web;
using testy.Models;

namespace testy.Helpers.Admin
{
    public class PytaniaConHelper
    {
        public static void PobierzPytaniaZBazy(int idWlasciciela, List<DodajPytanieModel> pytaniaDlaWidoku)
        {
            List<Pytania> pytaniaZBazy = new List<Pytania>();
            using (var db = new projektEntities())
            {
                pytaniaZBazy = (from i in db.Pytania
                                where i.PYT_wlascicielTestu_ID_FK == idWlasciciela
                                      && i.PYT_czyAktywne == true
                                select i).ToList();
            }
            foreach (var item in pytaniaZBazy)
            {
                DodajPytanieModel _temp = new DodajPytanieModel();
                _temp.PYT_IdPytania = int.Parse(item.PYT_ID.ToString());
                _temp.PYT_iloscPunktow = int.Parse(item.PYT_iloscPunktow.ToString());
                _temp.PYT_typ = item.PYT_typ;
                _temp.PYT_tresc = item.PYT_tresc;
                pytaniaDlaWidoku.Add(_temp);
            }
        }
        public static void ZapiszEdytowanePytanieWBazie(DodajPytanieModel daneZFormularza, int idWlasciciela)
        {
            using (var db = new projektEntities())
            {
                Pytania pytanieZBazy = (from i in db.Pytania
                                        where i.PYT_wlascicielTestu_ID_FK == idWlasciciela
                                              && i.PYT_ID == daneZFormularza.PYT_IdPytania
                                        select i).FirstOrDefault();
                if (pytanieZBazy != null)
                {
                    pytanieZBazy.PYT_iloscPunktow = daneZFormularza.PYT_iloscPunktow;
                    pytanieZBazy.PYT_tresc = daneZFormularza.PYT_tresc;
                    db.Pytania.AddOrUpdate(pytanieZBazy);
                    db.SaveChanges();

                    if (daneZFormularza.PYT_typ == "Otwarte")
                    {
                        Odpowiedzi odpowiedzZFormularza = new Odpowiedzi();
                        odpowiedzZFormularza.ODP_ID = daneZFormularza.odpowiedzId.First();
                        odpowiedzZFormularza.ODP_czyPoprawne = true;
                        odpowiedzZFormularza.ODP_pyt_ID_FK = daneZFormularza.PYT_IdPytania;
                        odpowiedzZFormularza.ODP_tresc = daneZFormularza.odpowiedzOtwarta;
                        db.Odpowiedzi.AddOrUpdate(odpowiedzZFormularza);
                    }
                    else
                    {
                        int ktora = 0;
                        foreach (var item in daneZFormularza.odpowiedz)
                        {
                            Odpowiedzi odpowiedzZFormularza = new Odpowiedzi();
                            if (daneZFormularza.zaznaczone.Contains((ktora + 1).ToString()))
                            {
                                odpowiedzZFormularza.ODP_czyPoprawne = true;
                            }
                            else
                            {
                                odpowiedzZFormularza.ODP_czyPoprawne = false;
                            }
                            odpowiedzZFormularza.ODP_pyt_ID_FK = daneZFormularza.PYT_IdPytania;
                            odpowiedzZFormularza.ODP_tresc = daneZFormularza.odpowiedz[ktora];
                            if (daneZFormularza.odpowiedzId.Count >= ktora + 1)
                                odpowiedzZFormularza.ODP_ID =
                                    int.Parse(daneZFormularza.odpowiedzId[ktora].ToString());
                            db.Odpowiedzi.AddOrUpdate(odpowiedzZFormularza);
                            ktora++;
                        }
                    }
                }
                db.SaveChanges();
            }
        }

        public static void zapiszPytanieWBazie(projektEntities db, DodajPytanieModel pytanie, int idWlasciciela)
        {
            var pytanieDoZapisu = new Pytania();

            pytanieDoZapisu.PYT_iloscPunktow = pytanie.PYT_iloscPunktow;
            pytanieDoZapisu.PYT_tresc = pytanie.PYT_tresc;
            pytanieDoZapisu.PYT_typ = pytanie.PYT_typ;
            pytanieDoZapisu.PYT_wlascicielTestu_ID_FK = idWlasciciela;
            pytanieDoZapisu.PYT_czyAktywne = true;
            db.Pytania.Add(pytanieDoZapisu);
            db.SaveChanges();

            var idOstatniegoPytania = (from i in db.Pytania
                                       where i.PYT_wlascicielTestu_ID_FK == idWlasciciela
                                       orderby i.PYT_ID descending
                                       select i.PYT_ID).FirstOrDefault();

            if (pytanie.PYT_typ == "Otwarte")
            {
                var odpDoZapisu = new Odpowiedzi();
                odpDoZapisu.ODP_pyt_ID_FK = idOstatniegoPytania;
                odpDoZapisu.ODP_tresc = pytanie.odpowiedz.First();
                odpDoZapisu.ODP_czyPoprawne = true;
                db.Odpowiedzi.Add(odpDoZapisu);
            }
            else
            {
                int ktora = 0;
                foreach (var item in pytanie.odpowiedz)
                {
                    ktora++;
                    if (!String.IsNullOrEmpty(item))
                    {
                        var odpDoZapisu = new Odpowiedzi();
                        odpDoZapisu.ODP_pyt_ID_FK = idOstatniegoPytania;
                        odpDoZapisu.ODP_tresc = item;
                        if (pytanie.zaznaczone.Contains(ktora.ToString()))
                        {
                            odpDoZapisu.ODP_czyPoprawne = true;
                        }
                        else
                        {
                            odpDoZapisu.ODP_czyPoprawne = false;
                        }
                        db.Odpowiedzi.Add(odpDoZapisu);
                    }
                }
            }
            db.SaveChanges();
        }

        public static void ZwrocPytaniaDoEdycji(int id, int idWlasciciela, DodajPytanieModel pytanieDlaWidoku)
        {
            using (var db = new projektEntities())
            {
                var pytanieZBazy = (from i in db.Pytania
                                    where i.PYT_ID == id
                                          && i.PYT_wlascicielTestu_ID_FK == idWlasciciela
                                          && i.PYT_czyAktywne == true
                                    select i).FirstOrDefault();
                if (pytanieZBazy == null)
                    return;
                pytanieDlaWidoku.PYT_IdPytania = int.Parse(pytanieZBazy.PYT_ID.ToString());
                pytanieDlaWidoku.PYT_iloscPunktow = int.Parse(pytanieZBazy.PYT_iloscPunktow.ToString());
                pytanieDlaWidoku.PYT_typ = pytanieZBazy.PYT_typ;
                pytanieDlaWidoku.PYT_tresc = pytanieZBazy.PYT_tresc;
                pytanieDlaWidoku.PYT_czyAktywne = true;
                var odpowiedziDlaPytania = (from c in db.Odpowiedzi
                                            where c.ODP_pyt_ID_FK == id
                                            select c).ToList();
                pytanieDlaWidoku.odpowiedz = new List<string>();
                pytanieDlaWidoku.odpowiedzId = new List<int>();
                pytanieDlaWidoku.zaznaczone = new List<string>();
                foreach (var item in odpowiedziDlaPytania)
                {
                    pytanieDlaWidoku.odpowiedzId.Add(int.Parse(item.ODP_ID.ToString()));
                    if (item.ODP_czyPoprawne)
                        pytanieDlaWidoku.zaznaczone.Add("true");
                    else
                        pytanieDlaWidoku.zaznaczone.Add("false");
                    pytanieDlaWidoku.odpowiedz.Add(item.ODP_tresc);
                    pytanieDlaWidoku.odpowiedzOtwarta = item.ODP_tresc;
                }
            }
        }
    }
}