﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Web;
using testy.Models;

namespace testy.Helpers.Admin
{
    public static class TestyConHelper
    {
        public static bool CzyDodacNowe(List<decimal> stare, string item)
        {
            return !(stare.Contains(int.Parse(item.ToString())));
        }
        /// <summary>
        /// czy item.id jest w ZFormulrza jesli nie to usun
        /// </summary>
        /// <param name="zFormularza"></param>
        /// <param name="item"></param>
        /// <returns></returns>
        public static bool SprawdzCzyUsunac(string[] zFormularza, string item)
        {
            return !(zFormularza.Contains(item));
        }

        public static void ZapisanieTestuWBazie(DodajTestModel daneZFormularza, int idWlasciciela)
        {
            test daneDoZapisu = new test();
            daneDoZapisu.TES_nazwa = daneZFormularza.TES_nazwa;
            daneDoZapisu.TES_aktywny = true;
            daneDoZapisu.TES_wlasciciel_ID_FK = idWlasciciela;
            daneDoZapisu.TES_dataUtworzenia = DateTime.Now;
            using (var db = new projektEntities())
            {
                db.test.Add(daneDoZapisu);
                db.SaveChanges();
            }
        }

        public static void ZwrocTestyZbazy(int idWlasciciela, List<DodajTestModel> testyZBazy)
        {
            using (var db = new projektEntities())
            {
                var znalezioneTesty = from i in db.test
                                      where i.TES_wlasciciel_ID_FK == idWlasciciela
                                      && i.TES_aktywny == true
                                      orderby i.TES_nrTestu descending 
                                      select i;
                foreach (var item in znalezioneTesty)
                {
                    DodajTestModel _temp = new DodajTestModel();
                    _temp.TES_nazwa = item.TES_nazwa;
                    _temp.TES_idTest = int.Parse(item.TES_nrTestu.ToString());
                    _temp.TES_dataUtworzenia = item.TES_dataUtworzenia;
                    testyZBazy.Add(_temp);
                }
            }
        }

        public static void UnieaktywnijWybranyTest(int id, int idWlasciciela)
        {
            using (var db = new projektEntities())
            {
                test doUsuniecia = (from i in db.test
                                    where i.TES_nrTestu == id
                                          && i.TES_wlasciciel_ID_FK == idWlasciciela
                                    select i).FirstOrDefault();
                if (doUsuniecia != null)
                {
                    doUsuniecia.TES_aktywny = false;
                    db.test.AddOrUpdate(doUsuniecia);
                    db.SaveChanges();
                }
            }
        }

        public static DodajTestModel ZwrocDaneTestuZBazy(int id, int idWlasciciela)
        {
            DodajTestModel testDoEdycji = new DodajTestModel();
            using (var db = new projektEntities())
            {
                test testZBazy = (from i in db.test
                                where i.TES_nrTestu == id
                                      && i.TES_wlasciciel_ID_FK == idWlasciciela
                                      && i.TES_aktywny == true
                                select i).FirstOrDefault();
                if (testZBazy == null)
                    return null;
                testDoEdycji.TES_idTest = int.Parse(testZBazy.TES_nrTestu.ToString());
                testDoEdycji.TES_nazwa = testZBazy.TES_nazwa;
                testDoEdycji.TES_dataUtworzenia = testZBazy.TES_dataUtworzenia;
                testDoEdycji.TES_czyAktywny = testZBazy.TES_aktywny;
                testDoEdycji.TES_idWlasciciela = int.Parse(testZBazy.TES_wlasciciel_ID_FK.ToString());
            }
            return testDoEdycji;
        }
    }
}