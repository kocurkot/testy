﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using testy.Models;

namespace testy.Helpers.Admin
{
    public static class PrawaConHelper
    {
        public static void PobierzTestyZbazy(projektEntities db, DodajPrawoModel dane, int idWlasciciela)
        {
            var testy = (from i in db.test
                         where i.TES_wlasciciel_ID_FK == idWlasciciela
                         && i.TES_aktywny == true
                         orderby i.TES_nrTestu descending 
                         select i).ToList();
            dane.PDT_Testy = new List<SelectListItem>();
            foreach (var item in testy)
            {
                string nazwaTestu;
                if (String.IsNullOrEmpty(item.TES_nazwa))
                    nazwaTestu = "";
                else
                    nazwaTestu = item.TES_nazwa;
                dane.PDT_Testy.Add(new SelectListItem
                {
                    Value = item.TES_nrTestu.ToString(),
                    Text = nazwaTestu
                });
            }
        }

        public static void PrawaNalezaceDoUzytkownika(projektEntities db, int idWlasciciela, List<PrawaDoTestu> prawaZTabeli)
        {
            var prawaUzytkownika = from i in db.PrawaDoTestu
                                   join e in db.test on i.PDT_test_ID_FK equals e.TES_nrTestu
                                   where e.TES_wlasciciel_ID_FK == idWlasciciela
                                   orderby i.PDT_id descending 
                                   select i;
            foreach (var item in prawaUzytkownika)
            {
                PrawaDoTestu krotka = new PrawaDoTestu();
                krotka.PDT_id = item.PDT_id;
                krotka.PDT_nazwaGrupy = item.PDT_nazwaGrupy;
                krotka.PDT_ileWypelniono = item.PDT_ileWypelniono;
                krotka.PDT_limitWypelnien = item.PDT_limitWypelnien;
                krotka.PDT_czasRozpoczecia = item.PDT_czasRozpoczecia;
                krotka.PDT_czasZakonczenia = item.PDT_czasZakonczenia;
                krotka.PDT_czasNaRozwizanie = item.PDT_czasNaRozwizanie / 60;
                prawaZTabeli.Add(krotka);
            }
        }
        
        public static void UpdatePrawaPrzepiszDaneZForm(DodajPrawoModel daneZFormularza, PrawaDoTestu daneDoUpdate)
        {
            daneDoUpdate.PDT_czasNaRozwizanie = daneZFormularza.PDT_czasNaWykonanieWMinutach * 60;
            daneDoUpdate.PDT_czasRozpoczecia = daneZFormularza.PDT_czasRozpoczecia;
            daneDoUpdate.PDT_czasZakonczenia = daneZFormularza.PDT_czasZakonczenia;
            daneDoUpdate.PDT_haslo = daneZFormularza.PDT_haslo.Trim();
            daneDoUpdate.PDT_id = daneZFormularza.idPrawa;
            daneDoUpdate.PDT_iloscPytan = daneZFormularza.PDT_iloscPytan;
            daneDoUpdate.PDT_ileWypelniono = daneZFormularza.ileWypelniono;
            daneDoUpdate.PDT_limitWypelnien = daneZFormularza.PDT_limitWypelnien;
            daneDoUpdate.PDT_nazwaGrupy = daneZFormularza.PDT_nazwaGrupy;
            daneDoUpdate.PDT_test_ID_FK = daneZFormularza.PDT_test_ID_FK;
        }

        public static void WypelnijModelPrawaDoEdycji(projektEntities db, DodajPrawoModel prawoDlaFormularza, int idWlasciciela,
            PrawaDoTestu znalezionePrawoDoTestu)
        {
            PrawaConHelper.PobierzTestyZbazy(db, prawoDlaFormularza, idWlasciciela);
            prawoDlaFormularza.PDT_WybranyTest = int.Parse(znalezionePrawoDoTestu.PDT_test_ID_FK.ToString());
            prawoDlaFormularza.PDT_czasNaWykonanieWMinutach =
                int.Parse((znalezionePrawoDoTestu.PDT_czasNaRozwizanie / 60).ToString());
            prawoDlaFormularza.PDT_czasRozpoczecia = znalezionePrawoDoTestu.PDT_czasRozpoczecia;
            prawoDlaFormularza.PDT_czasZakonczenia = znalezionePrawoDoTestu.PDT_czasZakonczenia;
            prawoDlaFormularza.PDT_haslo = znalezionePrawoDoTestu.PDT_haslo;
            prawoDlaFormularza.PDT_iloscPytan = int.Parse(znalezionePrawoDoTestu.PDT_iloscPytan.ToString());
            prawoDlaFormularza.PDT_limitWypelnien = int.Parse(znalezionePrawoDoTestu.PDT_limitWypelnien.ToString());
            prawoDlaFormularza.PDT_nazwaGrupy = znalezionePrawoDoTestu.PDT_nazwaGrupy;
            prawoDlaFormularza.PDT_test_ID_FK = int.Parse(znalezionePrawoDoTestu.PDT_test_ID_FK.ToString());
            prawoDlaFormularza.idPrawa = int.Parse(znalezionePrawoDoTestu.PDT_id.ToString());
        }

        public static void ZapiszPrawo(DodajPrawoModel daneZFormularza, PrawaDoTestu doZapisu, projektEntities db)
        {
            doZapisu.PDT_czasRozpoczecia = daneZFormularza.PDT_czasRozpoczecia;
            doZapisu.PDT_czasZakonczenia = daneZFormularza.PDT_czasZakonczenia;
            doZapisu.PDT_limitWypelnien = daneZFormularza.PDT_limitWypelnien;
            doZapisu.PDT_nazwaGrupy = daneZFormularza.PDT_nazwaGrupy;
            doZapisu.PDT_ileWypelniono = 0;
            doZapisu.PDT_iloscPytan = daneZFormularza.PDT_iloscPytan;
            doZapisu.PDT_czasNaRozwizanie = daneZFormularza.PDT_czasNaWykonanieWMinutach * 60;
            doZapisu.PDT_haslo = daneZFormularza.PDT_haslo;
            doZapisu.PDT_test_ID_FK = daneZFormularza.PDT_WybranyTest;
            db.PrawaDoTestu.Add(doZapisu);
            db.SaveChanges();
        }

    }
}